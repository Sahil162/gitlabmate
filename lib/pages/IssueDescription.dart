import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
//import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:gitlab_mate/pages/GetNotes.dart';

import 'package:http/http.dart' as http;

class Issue {
  final String issueId;
  final String projectId;
  final String issueTitle;
  final String issueDescription;
  final String issueState;
  final String projectAvatarUrl;
  Issue(
      {this.issueDescription,
      this.issueId,
      this.issueState,
      this.issueTitle,
      this.projectId,
      this.projectAvatarUrl});

  factory Issue.fromJson(Map<String, dynamic> json) {
    return Issue(
        projectId: json['project_id'].toString(),
        issueId: json['id'].toString(),
        issueDescription: json['description'].toString(),
        issueState: json['state'].toString(),
        issueTitle: json['title'].toString(),
        projectAvatarUrl: json['author.avatar_url'].toString());
  }
}

class IssueDescription extends StatefulWidget {
  final String token;
  final String issueId;
  final String title;
  final String issueDescription;
  final String issueState;
  final String projectId;

  IssueDescription(
      {this.issueId,
      this.token,
      this.title,
      this.issueDescription,
      this.issueState,
      this.projectId});
  @override
  _IssueDescriptionState createState() => _IssueDescriptionState();
}

class _IssueDescriptionState extends State<IssueDescription> {
  get issueDescription => null;
  Future<Issue> _futureIssue;
  Future<Issue> _fetchUserIssue(String token, String issueId) async {
    final jobsListAPIUrl =
        'https://gitlab.com/api/v4/issues?id=$issueId&private_token=$token';
    final response = await http.get(jobsListAPIUrl);
    //print(response.body);
    if (response.statusCode == 200) {
      final jsonresponse = json.decode(response.body);
      return Issue.fromJson(jsonresponse[0]);
    } else {
      throw Exception('Failed to load Issue');
    }
  }

  @override
  void initState() {
    super.initState();
    _futureIssue = _fetchUserIssue(widget.token, widget.issueId);
    //print(_futureIssue);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(
                height: 30.0,
              ),
              ListTile(
                title: RichText(
                  text: TextSpan(
                    text: widget.title,
                    style: new TextStyle(
                      fontSize: 18.0,
                      color: Colors.black87,
                    ),
                    children: <TextSpan>[
                      TextSpan(
                        text: " (" + widget.issueId + ")",
                        style: TextStyle(
                          fontSize: 15.0,
                          color: Colors.grey[500],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Container(
                child: FutureBuilder<Issue>(
                  future: _futureIssue,
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      return Card(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15.0),
                        ),
                        margin: EdgeInsets.all(15.0),
                        child: Column(
                          children: <Widget>[
                            SizedBox(
                              height: 15.0,
                            ),
                            ListTile(
                              title: Text(
                                widget.issueDescription,
                                style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.w300,
                                  fontSize: 14.0,
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 15.0,
                            ),
                            ListTile(
                              leading: Icon(
                                Icons.stacked_bar_chart,
                                color: Colors.blue,
                              ),
                              title: Text(
                                "Notes",
                                style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.w500,
                                  fontSize: 16.0,
                                ),
                              ),
                              trailing: Icon(
                                Icons.arrow_forward_ios,
                                size: 15,
                                color: Colors.grey[400],
                              ),
                              onTap: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => GetNotes(
                                      token: widget.token,
                                      projectId: widget.projectId,
                                      issueId: widget.issueId,
                                    ),
                                  ),
                                );
                              },
                            ),
                          ],
                        ),
                      );
                    } else if (snapshot.hasError) {
                      return Text("${snapshot.error}");
                    }
                    return Card(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15.0),
                      ),
                      margin: EdgeInsets.all(15.0),
                      child: Column(
                        children: <Widget>[
                          ListTile(
                            title: Center(
                              child: CircularProgressIndicator(),
                            ),
                            onTap: () {},
                          ),
                        ],
                      ),
                    );
                  },
                ),
              ),
              SizedBox(
                height: 20.0,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
