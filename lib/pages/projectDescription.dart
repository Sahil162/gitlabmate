import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
//import 'package:gitlab_mate/pages/GetNotes.dart';
import 'package:gitlab_mate/pages/ProjectCommitList.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';
//import 'package:font_awesome_flutter/font_awesome_flutter.dart';

//import 'CreateIssue.dart';
import 'ProjectIssue.dart';

class Project {
  final String projectId;
  final String projectAvatarUrl;
  final String projectName;
  final String pathWithNamespace;
  final String projectDescription;
  final String projectDefaultBranch;
  final String projectReadmeUrl;
  final String projectForksCount;
  final String projectStarCount;
  final String projectVisibility;
  final String openIssuesCount;

  Project({
    this.projectId,
    this.projectAvatarUrl,
    this.projectName,
    this.pathWithNamespace,
    this.projectDescription,
    this.projectDefaultBranch,
    this.projectReadmeUrl,
    this.projectForksCount,
    this.projectStarCount,
    this.projectVisibility,
    this.openIssuesCount,
  });

  factory Project.fromJson(Map<String, dynamic> json) {
    return Project(
      projectId: json['id'].toString(),
      projectAvatarUrl: json['avatar_url'].toString(),
      projectName: json['name'].toString(),
      pathWithNamespace: json['path_with_namespace'].toString(),
      projectDescription: json['description'].toString(),
      projectDefaultBranch: json['default_branch'].toString(),
      projectReadmeUrl: json['readme_url'].toString(),
      projectForksCount: json['forks_count'].toString(),
      projectStarCount: json['star_count'].toString(),
      projectVisibility: json['visibility'].toString(),
      openIssuesCount: json['openIssuesCount'].toString(),
    );
  }
}

class ProjectDescription extends StatefulWidget {
  ProjectDescription(
      {this.title,
      this.token,
      this.projectId,
      this.projectName,
      this.projectAvatar,
      this.issueId});

  final String title;
  final String token;
  final String projectId;
  final String projectName;
  final String projectAvatar;
  final String issueId;

  @override
  _ProjectDescriptionState createState() => _ProjectDescriptionState();
}

class _ProjectDescriptionState extends State<ProjectDescription> {
  Future<Project> _futureProject;

  Future<Project> fetchProject(String token, String projectId) async {
    final response = await http.get(
        'https://gitlab.com/api/v4/projects/$projectId?&private_token=$token');
    if (response.statusCode == 200) {
      return Project.fromJson(jsonDecode(response.body));
    } else {
      throw Exception('Failed to load User');
    }
  }

  @override
  void initState() {
    super.initState();
    _futureProject = fetchProject(widget.token, widget.projectId);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(
                height: 20.0,
              ),
              Hero(
                tag: widget.projectAvatar,
                child: Container(
                  margin: EdgeInsets.symmetric(vertical: 30),
                  padding: EdgeInsets.all(6),
                  height: 100,
                  width: 100,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: Colors.grey.withOpacity(.5),
                  ),
                  child: ClipRRect(
                    borderRadius: BorderRadius.all(Radius.circular(100.0)),
                    child: CachedNetworkImage(
                      width: 50.0,
                      height: 50.0,
                      fit: BoxFit.cover,
                      imageUrl: widget.projectAvatar,
                      placeholder: (context, url) =>
                          new CircularProgressIndicator(
                        valueColor: AlwaysStoppedAnimation(Colors.yellow[700]),
                      ),
                      errorWidget: (context, url, error) =>
                          new Icon(Icons.error),
                    ),
                  ),
                ),
              ),
              ListTile(
                title: Center(
                  child: Text(
                    widget.projectName,
                    style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.w400,
                      fontSize: 27.0,
                    ),
                  ),
                ),
                subtitle: Center(
                  child: Text(
                    "(" + widget.projectId + ")",
                    style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.w300,
                      fontSize: 15.0,
                    ),
                  ),
                ),
              ),
              Container(
                child: FutureBuilder<Project>(
                  future: _futureProject,
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      return Column(
                        children: [
                          Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(15.0),
                            ),
                            margin: EdgeInsets.all(10.0),
                            child: Column(
                              children: <Widget>[
                                SizedBox(
                                  height: 10.0,
                                ),
                                ListTile(
                                  title: Text(
                                    snapshot.data.projectDescription,
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.w300,
                                      fontSize: 14.0,
                                    ),
                                  ),
                                ),
                                SizedBox(height: 15.0),
                              ],
                            ),
                          ),
                          ListTile(
                            title: Row(
                              children: <Widget>[
                                Expanded(
                                  child: RaisedButton(
                                    onPressed: () {},
                                    child: Row(
                                      children: [
                                        Icon(
                                          Icons.star_border,
                                          size: 20.0,
                                        ),
                                        Text(
                                          " Star | " +
                                              snapshot.data.projectStarCount,
                                        ),
                                      ],
                                    ),
                                    color: Colors.black38,
                                    textColor: Colors.white,
                                    shape: RoundedRectangleBorder(
                                      borderRadius:
                                          new BorderRadius.circular(100.0),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  width: 10.0,
                                ),
                                Expanded(
                                  child: RaisedButton(
                                    onPressed: () {},
                                    child: Row(
                                      children: [
                                        Icon(
                                          Icons.account_tree_outlined,
                                          size: 20.0,
                                        ),
                                        Text(
                                          " Fork | " +
                                              snapshot.data.projectForksCount,
                                        ),
                                      ],
                                    ),
                                    color: Colors.black38,
                                    textColor: Colors.white,
                                    shape: RoundedRectangleBorder(
                                      borderRadius:
                                          new BorderRadius.circular(100.0),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(15.0),
                            ),
                            margin: EdgeInsets.all(10.0),
                            child: Column(
                              children: <Widget>[
                                ListTile(
                                  leading: Icon(
                                    Icons.info_outline,
                                    color: Colors.blue,
                                  ),
                                  title: Text(
                                    "Issues",
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.w500,
                                      fontSize: 16.0,
                                    ),
                                  ),
                                  trailing: Icon(
                                    Icons.arrow_forward_ios,
                                    size: 15,
                                    color: Colors.grey[400],
                                  ),
                                  onTap: () {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (context) => ProjectIssue(
                                          token: widget.token,
                                          projectId: widget.projectId,
                                          title: "Issues",
                                        ),
                                      ),
                                    );
                                  },
                                ),
                                Divider(
                                  color: Colors.grey[300],
                                  endIndent: 0.0,
                                  indent: 70.0,
                                ),
                                ListTile(
                                  leading: Icon(
                                    Icons.stacked_bar_chart,
                                    color: Colors.blue,
                                  ),
                                  title: Text(
                                    "Commits",
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.w500,
                                      fontSize: 16.0,
                                    ),
                                  ),
                                  trailing: Icon(
                                    Icons.arrow_forward_ios,
                                    size: 15,
                                    color: Colors.grey[400],
                                  ),
                                  onTap: () {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (context) => ProjectCommitList(
                                          token: widget.token,
                                          projectId: widget.projectId,
                                          title: "Commits",
                                        ),
                                      ),
                                    );
                                  },
                                ),
                              ],
                            ),
                          ),
                          /*
                          IconButton(
                            icon: Icon(Icons.add),
                            color: Colors.blue,
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => CreateIssue(
                                    token: widget.token,
                                    title: widget.title,
                                    projectId: widget.projectId,
                                  ),
                                ),
                              );
                            },
                          ),
                          */
                        ],
                      );
                    } else if (snapshot.hasError) {
                      return Text("${snapshot.error}");
                    }
                    return Card(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15.0),
                      ),
                      margin: EdgeInsets.all(15.0),
                      child: Column(
                        children: <Widget>[
                          ListTile(
                            title: Center(
                              child: CircularProgressIndicator(),
                            ),
                            onTap: () {},
                          ),
                        ],
                      ),
                    );
                  },
                ),
              ),
              SizedBox(
                height: 20.0,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
