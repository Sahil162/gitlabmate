import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:gitlab_mate/main.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'dart:convert';
import 'dart:async';

Future<User> fetchUser(String token) async {
  final response =
      await http.get('https://gitlab.com/api/v4/user?private_token=$token');
  if (response.statusCode == 200) {
    return User.fromJson(jsonDecode(response.body));
  } else {
    throw Exception('Failed to load User');
  }
}

class User {
  final String userId;
  final String avatarUrl;
  final String name;
  final String username;
  final String state;
  final String lastSignInAt;
  final String bio;
  final String jobTitle;
  final String location;
  final String webUrl;
  final String email;
  final String websiteUrl;
  final String skype;
  final String linkedin;
  final String twitter;
  final String organization;

  User({
    this.userId,
    this.avatarUrl,
    this.name,
    this.username,
    this.state,
    this.lastSignInAt,
    this.bio,
    this.jobTitle,
    this.location,
    this.webUrl,
    this.email,
    this.websiteUrl,
    this.skype,
    this.linkedin,
    this.twitter,
    this.organization,
  });

  factory User.fromJson(Map<String, dynamic> json) {
    return User(
      userId: json['id'].toString(),
      avatarUrl: json['avatar_url'].toString(),
      name: json['name'].toString(),
      username: json['username'].toString(),
      state: json['state'].toString(),
      lastSignInAt: json['last_sign_in_at'].toString(),
      bio: json['bio'].toString(),
      jobTitle: json['job_title'].toString(),
      location: json['location'].toString(),
      webUrl: json['web_url'].toString(),
      email: json['email'].toString(),
      websiteUrl: json['website_url'].toString(),
      skype: json['skype'].toString(),
      linkedin: json['linkedin'].toString(),
      twitter: json['twitter'].toString(),
      organization: json['organization'].toString(),
    );
  }
}

class ProfilePage extends StatefulWidget {
  ProfilePage(
      {this.title, this.token, this.userAvatar, this.userName, this.name});

  final String title;
  final String token;
  final String userAvatar;
  final String name;
  final String userName;

  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  Future<User> futureUser;
  @override
  void initState() {
    super.initState();
    futureUser = fetchUser(widget.token);
  }

  _logoutUser() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.clear();
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (context) => SplashScreen()),
        (Route<dynamic> route) => false);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(
                height: 20.0,
              ),
              Hero(
                tag: "user-avatar",
                child: Container(
                  margin: EdgeInsets.symmetric(vertical: 30),
                  padding: EdgeInsets.all(6),
                  height: 200,
                  width: 200,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: Colors.grey.withOpacity(.5),
                  ),
                  child: ClipRRect(
                    borderRadius: BorderRadius.all(Radius.circular(100.0)),
                    child: CachedNetworkImage(
                      width: 100.0,
                      height: 100.0,
                      fit: BoxFit.cover,
                      imageUrl: widget.userAvatar,
                      placeholder: (context, url) =>
                          new CircularProgressIndicator(
                        valueColor: AlwaysStoppedAnimation(Colors.yellow[700]),
                      ),
                      errorWidget: (context, url, error) =>
                          new Icon(Icons.error),
                    ),
                  ),
                ),
              ),
              ListTile(
                title: Center(
                  child: Text(
                    widget.name,
                    style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.w400,
                      fontSize: 27.0,
                    ),
                  ),
                ),
              ),
              ListTile(
                title: Center(
                  child: Text(
                    widget.userName,
                    style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.w200,
                      fontSize: 20.0,
                    ),
                  ),
                ),
              ),
              Container(
                child: FutureBuilder<User>(
                  future: futureUser,
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      return Card(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15.0),
                        ),
                        margin: EdgeInsets.all(15.0),
                        child: Column(
                          children: <Widget>[
                            ListTile(
                              leading: Icon(
                                Icons.ac_unit_sharp,
                                size: 18.0,
                                color: Colors.blue,
                              ),
                              title: Text(
                                "Bio: " + snapshot.data.bio,
                                style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.w300,
                                  fontSize: 14.0,
                                ),
                              ),
                            ),
                            ListTile(
                              leading: Icon(
                                Icons.email,
                                size: 18.0,
                                color: Colors.red,
                              ),
                              title: Text(
                                snapshot.data.email,
                                style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.w300,
                                  fontSize: 14.0,
                                ),
                              ),
                            ),
                            ListTile(
                              leading: FaIcon(
                                FontAwesomeIcons.checkCircle,
                                size: 18.0,
                                color: Colors.green,
                              ),
                              title: Text(
                                snapshot.data.state,
                                style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.w300,
                                  fontSize: 14.0,
                                ),
                              ),
                            ),
                            ListTile(
                              leading: FaIcon(
                                FontAwesomeIcons.mapMarkerAlt,
                                size: 18.0,
                                color: Colors.blueGrey,
                              ),
                              title: Text(
                                snapshot.data.location,
                                style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.w300,
                                  fontSize: 14.0,
                                ),
                              ),
                            ),
                            ListTile(
                              leading: FaIcon(
                                FontAwesomeIcons.linkedinIn,
                                size: 18.0,
                                color: Colors.blue,
                              ),
                              title: Text(
                                snapshot.data.linkedin,
                                style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.w300,
                                  fontSize: 14.0,
                                ),
                              ),
                            ),
                            ListTile(
                              leading: FaIcon(
                                FontAwesomeIcons.twitter,
                                size: 18.0,
                                color: Colors.blue,
                              ),
                              title: Text(
                                snapshot.data.twitter,
                                style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.w300,
                                  fontSize: 14.0,
                                ),
                              ),
                            ),
                            SizedBox(height: 20),
                          ],
                        ),
                      );
                    } else if (snapshot.hasError) {
                      return Text("${snapshot.error}");
                    }
                    return Card(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15.0),
                      ),
                      margin: EdgeInsets.all(15.0),
                      child: Column(
                        children: <Widget>[
                          ListTile(
                            title: Center(
                              child: CircularProgressIndicator(),
                            ),
                            onTap: () {},
                          ),
                        ],
                      ),
                    );
                  },
                ),
              ),
              SizedBox(
                height: 20.0,
              ),
              /* Logout Button */
              GestureDetector(
                onTap: () {
                  _logoutUser();
                },
                child: Padding(
                  padding: EdgeInsets.only(bottom: 30),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        padding:
                            EdgeInsets.symmetric(vertical: 20, horizontal: 27),
                        decoration: BoxDecoration(
                          color: Colors.red.withOpacity(.91),
                          borderRadius: BorderRadius.circular(27),
                        ),
                        child: Row(
                          children: <Widget>[
                            Text(
                              "Logout",
                              style: new TextStyle(
                                fontSize: 16.0,
                                color: Colors.white,
                              ),
                            ),
                            SizedBox(width: 30),
                            Icon(
                              Icons.power_settings_new,
                              color: Colors.white,
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
